import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

injectTapEventPlugin();

import App from './components/app';


ReactDOM.render(
    <MuiThemeProvider>
      <App /> 
    </MuiThemeProvider>
  , document.querySelector('.container'));
